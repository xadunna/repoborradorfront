import {html, PolymerElement} from '@polymer/polymer/polymer-element.js';
import '@polymer/iron-ajax/iron-ajax.js';


/**
 * @customElement
 * @polymer
 */
class ReceptorEvento extends PolymerElement {
  static get template() {
    return html`
      <style>
        :host {
          display: block;
        }
      </style>
      <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
      <h2>Soy el receptor</h2>
      <h3>Este curso es [[course]]</h3>
      <h3>y estamos en el año [[year]]</h3>
      <input type="text" value="{{course::input}}"/>
    `;
  }
  static get properties() {
    return {
      course:{
        type: String,
        observer: "_courseChanged" //añadimos función observadora
      },
      year:{
        type: String
      }
    };
  }// End properties

  _courseChanged(newValue, oldValue){
    console.log("Course value has changed");
    console.log("Old value was " + oldValue);
    console.log("New value is " + newValue);
  }
}//End class

window.customElements.define('receptor-evento', ReceptorEvento);
