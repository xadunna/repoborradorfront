
import {html, PolymerElement} from '@polymer/polymer/polymer-element.js';

import '@polymer/iron-ajax/iron-ajax.js';

/**
 * @customElement
 * @polymer
 */
class VisorCuenta extends PolymerElement {
  static get template() {
    return html`
      <style>
        :host {
          display: block;
          /*all: initial;*/
            border: solid blue;
        }
        .redbg{
          background-color: red;
        }
        .greenbg {
          background-color: green;
        }
        .bluebg {
          background-color: blue;
        }
        .greybg {
          background-color: grey;
        }
      </style>
      <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
      <table class="table">
        <thead>
          <tr>
            <th scope="col">Account</th>
            <th scope="col">Last Balance</th>
          </tr>
        </thead>
        <tbody>
          <template is="dom-repeat" items="{{accounts}}">
            <tr>
              <td>{{item.IBAN}}</td>
              <td>{{item.balance}}</td>
            </tr>
          </template>
          <!--dom-repeat items="{{accounts}}">
          <template>
            <tr>
              <td>{{item.IBAN}}</td>
              <td>{{item.balance}}</td>
            </tr>
          </template>
          </dom-repeat-->
      </tbody>
 </table>

      <iron-ajax
       method="GET"
       id="getAccount"
       url="http://localhost:3000/apitechu/v2/accounts/{{userid}}"
       handle-as="json"
       on-response="showData"
       on-error="showError"
      >
      </iron-ajax>
    `;
  }
  static get properties() {
    return {
      accounts: {
        type: Array
      },
      userid: {
        type: Number,
        observer: "_userIdChanged"
      }

    };
  } //End properties
//La función manejadora tiene un único argumento
 showData(data){
   console.log("showData");
   console.log(data.detail.response);
   this.accounts=data.detail.response;


}
showError(error){
  console.log("Hubo un error");
  console.log(error);
}
_userIdChanged(newValue, oldValue){
  console.log("UserId of visor-cuenta value has changed");
  console.log("Old value was " + oldValue);
  console.log("New value is " + newValue);
  //Con el $ referencio a un componente que está dentro del html
  this.$.getAccount.userid=JSON.stringify(newValue); //la variable en memoria es binaria
  this.$.getAccount.generateRequest(); //lanza la petición
}
}// End class

window.customElements.define('visor-cuenta', VisorCuenta);
