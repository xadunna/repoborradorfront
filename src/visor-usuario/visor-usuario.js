
import {html, PolymerElement} from '@polymer/polymer/polymer-element.js';
import '@polymer/iron-ajax/iron-ajax.js';
import '../visor-cuenta/visor-cuenta.js';
/**
 * @customElement
 * @polymer
 */
class VisorUsuario extends PolymerElement {
  static get template() {
    return html`
      <style>
        :host {
          display: block;
          /*all: initial;*/
            border: solid blue;
        }
        .redbg{
          background-color: red;
        }
        .greenbg {
          background-color: green;
        }
        .bluebg {
          background-color: blue;
        }
        .greybg {
          background-color: grey;
        }
      </style>
      <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
      <!--div class="row greybg">
        <div class="col-2  offset-1 redbg">Col 1</div>
        <div class="col-3  offset-1 greenbg">Col 2</div>
        <div class="col-4  bluebg">Col 3</div>
      </div-->
      <!--button class="btn btn-info">Login</button>
      <button class="btn btn-success">Logout</button>
      <button class="btn btn-danger">Login</button>
      <button class="btn btn-warning">Logout</button>
      <button class="btn btn-primary btn-lg">Login</button>
      <button class="btn btn-secondary btn-sm">Logout</button>
      <button class="btn btn-light">Login</button>
      <button class="btn btn-dark">Logout</button-->
      <!--h2>Soy [[first_name]] [[last_name]]</h2>
      <h2>y mi email es [[email]]</h2-->


      <h3>Bienvenido [[first_name]] [[last_name]]</h3>
      <h3>Su email es [[email]]</h3>
      <button class="btn btn-info" on-click="verCuentas">Ver cuentas</button>
      <iron-ajax
       id="getUser"
       url="http://localhost:3000/apitechu/v2/users/{{userid}}"
       handle-as="json"
       on-response="showData"
       on-error="showError"
       method="GET"
      >
      </iron-ajax>
      <visor-cuenta id="account" hidden$="[[!areAccounts]]"></visor-cuenta>

    `;
  }
  static get properties() {
    return {
      first_name: {
        type: String
      },
      last_name: {
        type: String
      },
      email: {
        type: String
      },
      userid: {
        type: Number,
        observer: "_userIdChanged" //añadimos función observadora
      },
      areAccounts: {
        type: Boolean,
        value: false
      }
    };
  } //End properties
//La función manejadora tiene un único argumento
 showData(data){
   console.log("showData");
   console.log(data.detail.response);

   this.first_name=data.detail.response.first_name;
   this.last_name=data.detail.response.last_name;
   this.email=data.detail.response.email;

}
showError(error){
  console.log("Hubo un error");
  console.log(error);
}
_userIdChanged(newValue, oldValue){
  console.log("UserId value has changed");
  console.log("Old value was " + oldValue);
  console.log("New value is " + newValue);
  //Con el $ referencio a un componente que está dentro del html
  this.$.getUser.id=JSON.stringify(newValue); //la variable en memoria es binaria
  this.$.getUser.generateRequest(); //lanza la petición

}
verCuentas(){
  this.areAccounts=true;
  this.$.account.userid=this.userid;
}
}// End class

window.customElements.define('visor-usuario', VisorUsuario);
