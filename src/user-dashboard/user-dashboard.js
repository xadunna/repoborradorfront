import {html, PolymerElement} from '@polymer/polymer/polymer-element.js';
import '@polymer/iron-ajax/iron-ajax.js';
import '@polymer/iron-pages/iron-pages.js';
import '../login-usuario/login-usuario.js';
import '../visor-usuario/visor-usuario.js';



/**
 * @customElement
 * @polymer
 */
class UserDashboard extends PolymerElement {
  static get template() {
    return html`
      <style>
        :host {
          display: block;
        }
      </style>
      <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">

      <iron-pages selected="[[componentName]]" attr-for-selected="component-name">
        <div component-name="login-usuario"><login-usuario on-myevent="processEvent"></login-usuario></div>
        <div component-name="visor-usuario"><visor-usuario id="receiver"></visor-usuario></div>
      </iron-pages>
    `;
  }
  static get properties() {
    return {
      componentName: {
        type: String,
        value: "login-usuario"
      }
    };
  }// End properties
  processEvent(e){
    console.log("Capturado evento de login-usuario");
    console.log(e);
    console.log(e.detail.idusuario);
    this.$.receiver.userid=e.detail.idusuario;
    this.componentName="visor-usuario";


  }
}//End class

window.customElements.define('user-dashboard', UserDashboard);
