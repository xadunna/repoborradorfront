import {html, PolymerElement} from '@polymer/polymer/polymer-element.js';
import '@polymer/iron-ajax/iron-ajax.js';


/**
 * @customElement
 * @polymer
 */
class LoginUsuario extends PolymerElement {
  static get template() {
    return html`
      <style>
        :host {
          display: block;
        }
      </style>
      
      <h2>Por favor, introduzca los datos</h2>
      <input type="email" placeholder="email" value="{{email::input}}" />
      <br/>
      <input type="password" placeholder="password" value="{{password::input}}"/>
      <br/>
      <button on-click="login">Login</button>
      <span hidden$="[[!isLogged]]">Bienvenido/a de nuevo</span>
      <iron-ajax
        id="doLogin"
        url="http://localhost:3000/apitechu/v2/login"
        handle-as="json"
        content-type="application/json"
        method="POST"
        on-response="manageAJAXresponse"
        on-error="showError"
      >
      </iron-ajax>
    `;
  }
  static get properties() {
    return {
      email: {
        type: String
      },
      password: {
        type: String
      },
      isLogged: {
        type: Boolean,
        value: false
      }
    };
  }// End properties
  login(){
    console.log("El usuario ha pulsado el botón");
    var loginData ={
        "email": this.email,
        "password": this.password
    }
    /*console.log("login data es " + loginData); //NO USAR CONCATENADO PARA SACAR LA INFO DEL OBJECT*/
    //console.log(loginData);

    //Con el $ referencio a un componente que está dentro del html
    this.$.doLogin.body=JSON.stringify(loginData); //la variable en memoria es binaria
    this.$.doLogin.generateRequest(); //lanza la petición

  }
  //100,200 y 300
  manageAJAXresponse(data){
    console.log("Llegaron los resultados");
    console.log(data.detail.response);
    this.isLogged =true;
    //lanzamos evento con id de usuario
    console.log("Lanzamos evento desde login-usuario");
    this.dispatchEvent(
          new CustomEvent(
            "myevent",
            {
              "detail" : {
                "idusuario": data.detail.response.id
              }//hasta aquí la estructura es fija
            }//object siempre es CustomEvent
          )//customEvent
        )//dispatch
  }
  showError(error){
    console.log("Hubo un error");
    console.log(error);
  }
}//End class

window.customElements.define('login-usuario', LoginUsuario);
